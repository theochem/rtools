Additional tools
================

Overview over all scripts and small tools in the folder *tools*. Those scripts are either totally independent of the *rtools* module, or provide additional command line features.

Contents:

.. toctree::
   :maxdepth: 2

   scripts/myq.rst
   scripts/menu.rst
   scripts/get_compute_nodes.rst
   scripts/aims_memtrace.rst
