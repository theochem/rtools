# Change Log
All notable changes to this project will be documented in this file from June 2017 on.
This project adheres to [gitlab/rtools](https://gitlab.lrz.de/theochem/rtools).

## [unreleased]
### Added
- ``exclude_nodes'' feature for arthur submit agents
- supermuc submit agents for CASTEP and AIMS (incl. test coverage)
- lrzlinuxcluster agents can work with job-dependencies
- `kpointdensity` task for convergence tests + support for more sets of PPs

### Changed
- behavior of setup.py: in case of a debian jessie image skip installation
### Fixed



## [0.2.0] -- 2017-05-09
Version presented in group seminar
